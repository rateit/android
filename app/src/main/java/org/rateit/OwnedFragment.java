package org.rateit;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.rateit.R;
import com.squareup.picasso.Picasso;

import org.rateit.Models.CD;
import org.rateit.Models.CDResponse;
import org.rateit.Models.OwnedAlbumMap;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;

public class OwnedFragment extends Fragment {
    public RatingBar ratingBar;
    private List<CD> cds;
    private ListView listView;
    private List<OwnedAlbumMap> ownedAlbumMap;
    private CustomAdapter customAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_view, container, false);
        //addListenerOnRatingBar(view);
        listView = (ListView) view.findViewById(R.id.listView);
        responseCDCall();
        return view;
    }

    public void responseCDCall() {
        Call<List<OwnedAlbumMap>> call = RetrofitClient
                .getInstance().getUserClient().getOwnedCD(token, "users", LoginActivity.id);

        call.enqueue(new Callback<List<OwnedAlbumMap>>() {
            @Override
            public void onResponse(Call<List<OwnedAlbumMap>> call, Response<List<OwnedAlbumMap>> response) {
                if (!response.isSuccessful()) {
//                    textViewResult.setText("Code: " + response.code());
                    System.out.println("Code: " + response.code());
                    return;
                } else {
                    if(response.body() != null) {
                         ownedAlbumMap = response.body();
                         customAdapter = new CustomAdapter(ownedAlbumMap);
                        listView.setAdapter(customAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<OwnedAlbumMap>> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
                System.out.println("NIE poszlo");
                System.out.println(t.getMessage());
            }
        });
    }
    @Override
    public void onResume() {
        Log.e("DEBUG", "onResume of HomeFragment");
        responseCDCall();
        super.onResume();
    }

    public class CustomAdapter extends BaseAdapter {
        private List<OwnedAlbumMap> ownedAlbumMap;

        public CustomAdapter(List<OwnedAlbumMap> ownedAlbumMap) {
            this.ownedAlbumMap = ownedAlbumMap;
        }

        @Override
        public int getCount() {
            if (ownedAlbumMap != null) {
                return ownedAlbumMap.size();
            }
            else
                return 0;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            ViewHolder holder;
                convertView = getLayoutInflater().inflate(R.layout.fragment_owned, null);
                holder = new ViewHolder();

                holder.artistTextView = (TextView) convertView.findViewById(R.id.artistName);
                holder.cdNameTextView = (TextView) convertView.findViewById(R.id.cdName);
                holder.imageView = (ImageView) convertView.findViewById(R.id.CDImage);
                holder.ratingBarView = (RatingBar) convertView.findViewById(R.id.ratingBar);
                holder.deleteButton = (Button) convertView.findViewById(R.id.delete_button);
                holder.detailsButton = (Button) convertView.findViewById(R.id.detailsButton);
                holder.giveButton = (Button) convertView.findViewById(R.id.give_cd);
                holder.cdType = (TextView) convertView.findViewById(R.id.cd_type);

            holder.cdNameTextView.setText(ownedAlbumMap.get(i).getCd().getName());
            Picasso.get().load(ownedAlbumMap.get(i).getCd().getPhotoURL()).into(holder.imageView);
            holder.ratingBarView.setRating(ownedAlbumMap.get(i).getCd().getRating());
            holder.artistTextView.setText(ownedAlbumMap.get(i).getCd().getArtist());
            holder.cdType.setText(ownedAlbumMap.get(i).getCdType().getType());

            holder.giveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        giveActivityPage(ownedAlbumMap.get(i).getId());
                    }
                });
                holder.detailsButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        long cdId = ownedAlbumMap.get(i).getCd().getId();
                        Call<CDResponse> call = RetrofitClient
                                .getInstance()
                                .getUserClient()
                                .getDataCDById(token, "cds", cdId);
                        call.enqueue(new Callback<CDResponse>() {
                            @Override
                            public void onResponse(Call<CDResponse> call, Response<CDResponse> response) {
                                String s = null;
                                try {
                                    if (!response.isSuccessful()) {
                                        s = response.errorBody().string();
                                    }
                                    CDResponse cd = response.body();
                                    detailsActivityPage(cd);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Call<CDResponse> call, Throwable t) {
                                System.out.println(":(");
                            }
                        });
                    }
                });


                holder.deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        long cdId = ownedAlbumMap.get(i).getId();
                        Call<ResponseBody> call = RetrofitClient
                                .getInstance()
                                .getUserClient()
                                .deleteCDFromOwned(token, "user", cdId);
                        call.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                System.out.println(response.code());
                                if(response.isSuccessful()) {
                                    responseCDCall();
                                    Toast.makeText(getActivity(), "Delete from owned succesfully", Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
                                System.out.println("NIE poszlo");
                                System.out.println(t.getMessage());
                            }
                        });
                    }
                });
                convertView.setTag(holder);
            return convertView;
        }
    }

    static class ViewHolder {
        Button deleteButton, detailsButton, giveButton;
        TextView artistTextView, cdNameTextView, cdType;
        ImageView imageView;
        RatingBar ratingBarView;
    }

    public void detailsActivityPage(CDResponse cd) {
        Intent intent = new Intent(getActivity(), CDDetailsActivity.class);
        intent.putExtra("CD", cd);
        startActivity(intent);
    }
    public void giveActivityPage(long ownedId) {
        Intent intent = new Intent(getActivity(), GiveActivity.class);
        intent.putExtra("Id", ownedId);
        startActivity(intent);
    }
}
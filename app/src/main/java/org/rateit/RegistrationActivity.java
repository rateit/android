package org.rateit;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.rateit.R;

import org.rateit.Models.Registration;
import org.rateit.Models.User;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editPasswordConfirm, editEmail, editPassword, editUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_registration);

        editUsername = (EditText) findViewById(R.id.editText4);
        editEmail = (EditText) findViewById(R.id.editText2);
        editPassword = (EditText) findViewById(R.id.editText);
        editPasswordConfirm = (EditText) findViewById(R.id.editText3);

        findViewById(R.id.button).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
    }
    boolean isEmail(EditText text) {
        CharSequence email = text.getText().toString();
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    void checkDataEntered() {
        if (isEmpty(editUsername)) {
            Toast t = Toast.makeText(this, "You must enter username to register!", Toast.LENGTH_SHORT);
            t.show();
        }

        if (isEmpty(editPassword)) {
            editPassword.setError("password is required!");
        }
        if (isEmpty(editPasswordConfirm)) {
            editPasswordConfirm.setError("password confirm is required!");
        }

        if (isEmail(editEmail) == false) {
            editEmail.setError("Enter valid email!");
        }

    }

    private void userSignUp(View v) {
        String email = editEmail.getText().toString().trim();
        String password = editPassword.getText().toString().trim();
        String username = editUsername.getText().toString().trim();
        String confirmPassword = editPasswordConfirm.getText().toString().trim();
        Call<User> call = RetrofitClient
                .getInstance()
                .getUserClientRegistration()
                .createUser(new Registration(confirmPassword, email, password, username));
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                String s = null;
                try {
                    checkDataEntered();
                    if (!response.isSuccessful()) {
                        s = response.errorBody().string();
                        Toast.makeText(RegistrationActivity.this, s, Toast.LENGTH_LONG).show();
                    } else {
                        System.out.println(response.code());
                        loginPageView(v);
                        Toast.makeText(RegistrationActivity.this, "Witaj " + username, Toast.LENGTH_LONG).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(RegistrationActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                System.out.println(":(");
            }
        });
    }

    public void loginPageView(View view) {
        startActivity(new Intent(this, LoginActivity.class));
    }

    public void cancelClick(View view) {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                userSignUp(v);
                break;
            case R.id.button2:
                cancelClick(v);
                break;
        }

    }
}

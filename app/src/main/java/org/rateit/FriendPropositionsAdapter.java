package org.rateit;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rateit.R;
import com.squareup.picasso.Picasso;

import org.rateit.Models.FriendRequest;
import org.rateit.Models.User;
import org.rateit.Models.UserProposition;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;

public class FriendPropositionsAdapter extends RecyclerView.Adapter<FriendPropositionsAdapter.MyViewHolder> {
    private Context context;
    private List<UserProposition> list;


    public FriendPropositionsAdapter() {
    }

    public FriendPropositionsAdapter(List<UserProposition> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.proposal_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //holder.button.setId(position);
        Picasso.get().load(list.get(position).getPhotoURL()).into(holder.proposalFriendAvatar);
        holder.proposalFriendUsername.setText(list.get(position).getNick());
        holder.invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                long receiverId = list.get(position).getId();
                Call<FriendRequest> call = RetrofitClient
                        .getInstance()
                        .getUserClient()
                        .sendRequest(token, receiverId, new FriendRequest(LoginActivity.user, "SENT", false));
                call.enqueue(new Callback<FriendRequest>() {
                    @Override
                    public void onResponse(Call<FriendRequest> call, Response<FriendRequest> response) {
                        String s = null;
                        try {
                            if (!response.isSuccessful()) {
                                s = response.errorBody().string();
                            } else {
                                Toast.makeText(context, "Send request", Toast.LENGTH_LONG).show();
                            }
                            System.out.println(response.code());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<FriendRequest> call, Throwable t) {
                        System.out.println(":(");
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        Button invite;
        TextView proposalFriendUsername;
        ImageView proposalFriendAvatar;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            invite = itemView.findViewById(R.id.add_friend);
            proposalFriendUsername = itemView.findViewById(R.id.proposal_friend_nick);
            proposalFriendAvatar = itemView.findViewById(R.id.friend_avatar);
        }
    }

    public void friendsDetailsPage(User user) {
        Intent intent = new Intent(context, FriendsDetailsActivity.class);
        intent.putExtra("USER", user);
        context.startActivity(intent);
    }
}

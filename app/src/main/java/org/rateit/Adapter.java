package org.rateit;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rateit.R;
import com.facebook.drawee.view.SimpleDraweeView;

import org.rateit.Models.CD;
import org.rateit.Models.CDResponse;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

    private Context context;
    private List<CD> list;
    private org.rateit.Models.Context contextPages;


    public Adapter() {
    }

    public Adapter(List<CD> list, org.rateit.Models.Context contextPages, Context context) {
        this.list = list;
        this.contextPages = contextPages;
        this.context = context;
    }

    public void setList(List<CD> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void addAll(List<CD> newList) {
        int lastIndex = list.size() - 1;
        list.addAll(newList);
        notifyItemRangeInserted(lastIndex, newList.size());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_home, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.cdNameTextView.setText("" + list.get(position).getName());
        holder.imageView.setImageURI(Uri.parse(list.get(position).getPhotoURL()));
        holder.ratingBarView.setRating(list.get(position).getRating());
        holder.artistTextView.setText(list.get(position).getArtist());
        holder.detailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                long cdId = list.get(position).getId();
                Call<CDResponse> call = RetrofitClient
                        .getInstance()
                        .getUserClient()
                        .getDataCDById(token, "cds", cdId);
                call.enqueue(new Callback<CDResponse>() {
                    @Override
                    public void onResponse(Call<CDResponse> call, Response<CDResponse> response) {
                        String s = null;
                        try {
                            if (!response.isSuccessful()) {
                                s = response.errorBody().string();
                            }
                            CDResponse cd = response.body();
                            detailsActivityPage(cd);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<CDResponse> call, Throwable t) {
                        System.out.println(":(");
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        Button detailsButton;
        TextView artistTextView, cdNameTextView;
        SimpleDraweeView imageView;
        RatingBar ratingBarView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            detailsButton = itemView.findViewById(R.id.detailsButton);
            artistTextView = itemView.findViewById(R.id.artistName);
            cdNameTextView = itemView.findViewById(R.id.cdName);
            imageView = (SimpleDraweeView) itemView.findViewById(R.id.CDImage);
            ratingBarView = itemView.findViewById(R.id.ratingBar);
        }
    }

    public void detailsActivityPage(CDResponse cd) {
        Intent intent = new Intent(context, CDDetailsActivity.class);
        intent.putExtra("CD", cd);
        context.startActivity(intent);
    }
}

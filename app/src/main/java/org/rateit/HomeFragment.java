package org.rateit;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rateit.R;
import com.facebook.drawee.backends.pipeline.Fresco;

import org.rateit.Models.CD;
import org.rateit.Models.CDAllResponse;
import org.rateit.Models.Context;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;


public class HomeFragment extends Fragment {
    public RatingBar ratingBar;
    private List<CD> cds;
    private Context context;
    private int page = 1;
    private Adapter adapter;
    private CDAllResponse cdAllResponse;
    private List<CD> cdsFromSearch;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    ProgressBar progressBar;
    boolean mIsLoading = false;
    boolean mIsLastPage = false;
    final int pageSize = 10;
    int mCurrentPage = 0;
    public static final String API_URL = "http://s416103.projektstudencki.pl:8080/";

    public List<CD> getCds() {
        return cds;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Fresco.initialize(getActivity());
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.relative_view, container, false);

        ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);

        progressBar = view.findViewById(R.id.progress_searching);
        recyclerView = view.findViewById(R.id.recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        searchCDCall("", true);


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                // number of visible items
                int visibleItemCount = layoutManager.getChildCount();
                // number of items in layout
                int totalItemCount = layoutManager.getItemCount();
                // the position of first visible item
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                boolean isNotLoadingAndNotLastPage = !mIsLoading && !mIsLastPage;
                // flag if number of visible items is at the last
                boolean isAtLastItem = firstVisibleItemPosition + visibleItemCount >= totalItemCount;
                // validate non negative values
                boolean isValidFirstItem = firstVisibleItemPosition >= 0;
                // validate total items are more than possible visible items
                boolean totalIsMoreThanVisible = totalItemCount >= pageSize;
                // flag to know whether to load more
                boolean shouldLoadMore = isValidFirstItem && isAtLastItem && totalIsMoreThanVisible && isNotLoadingAndNotLastPage;
                if (shouldLoadMore) {
                    searchCDCall("", false);
                    recyclerView.setAdapter(adapter);
                }
            }
        });
        ((MainActivity) getActivity()).setOnBackClickListener(new MainActivity.OnBackClickListener() {
            @Override
            public boolean onBackClick() {

                Intent a = new Intent(Intent.ACTION_MAIN);
                a.addCategory(Intent.CATEGORY_HOME);
                a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);

                return true;
            }
        });

        return view;
    }

    public void searchCDCall(String name, boolean isFirstPage) {
        if (name.equals("")) {

            mIsLoading = true;
            mCurrentPage = mCurrentPage + 1;


            Call<CDAllResponse> call = RetrofitClient
                    .getInstance().getUserClient().getDataCDWithPagination(token, "cds", mCurrentPage);

            call.enqueue(new Callback<CDAllResponse>() {
                @Override
                public void onResponse(Call<CDAllResponse> call, Response<CDAllResponse> response) {
                    if (!response.isSuccessful()) {
//                    textViewResult.setText("Code: " + response.code());
                        System.out.println("Code: " + response.code());
                        return;
                    } else {
                        if (response.body() == null) {
                            return;
                        }
                        if (adapter == null) {
                            cdsFromSearch = response.body().getCds();
                            adapter = new Adapter(cdsFromSearch, context, getActivity());
                        } else if (!isFirstPage) {
                            adapter.addAll(response.body().getCds());
                        } else {
                            adapter.setList(response.body().getCds());
                        }
                        progressBar.setVisibility(View.GONE);
                        adapter = new Adapter(cdsFromSearch, context, getActivity());
                        recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                        mIsLoading = false;
                        mIsLastPage = mCurrentPage == response.body().getContext().getTotalPages();
                    }
                }

                @Override
                public void onFailure(Call<CDAllResponse> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
                    progressBar.setVisibility(View.GONE);
                    System.out.println("NIE poszlo");
                    System.out.println(t.getMessage());
                }
            });
        } else {
            searchingResponses(name, isFirstPage);
        }
    }

    public void searchingResponses(String name, boolean isFirstPage) {
        Call<CDAllResponse> call = RetrofitClient
                .getInstance().getUserClient().searchCDByName(token, "cds", name);

        call.enqueue(new Callback<CDAllResponse>() {
            @Override
            public void onResponse(Call<CDAllResponse> call, Response<CDAllResponse> response) {
                if (!response.isSuccessful()) {
//                    textViewResult.setText("Code: " + response.code());
                    System.out.println("Code: " + response.code());
                    return;
                } else {
                    if (cdsFromSearch == null) {
                        cdsFromSearch = new ArrayList<CD>();
                        context = response.body().getContext();
                        cdsFromSearch = response.body().getCds();
                    } else {
                        cdsFromSearch = response.body().getCds();
                    }
                    progressBar.setVisibility(View.GONE);
                    adapter = new Adapter(cdsFromSearch, context, getActivity());
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<CDAllResponse> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
                progressBar.setVisibility(View.GONE);
                System.out.println("NIE poszlo");
                System.out.println(t.getMessage());
            }
        });


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();

        inflater.inflate(R.menu.menu_item, menu);
        MenuItem item = menu.findItem(R.id.search);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItem.SHOW_AS_ACTION_IF_ROOM);

        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                searchCDCall(query, true);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // Here is where we are going to implement the filter logic
                searchCDCall(newText, true);
                return true;
            }

        });
    }

}
package org.rateit;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rateit.R;

import org.rateit.Models.Chat;
import org.rateit.Models.Message;
import org.rateit.Models.NewMessage;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;

public class MessagingActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView mMessageRecycler;
    private MessageListAdapter mMessageAdapter;
    private Chat chat;
    private Button sendMessage;
    private List<Message> messageList;
    private EditText messageEditText;
    private final Handler handler = new Handler();
    private MessageListAdapter messageListAdapter = new MessageListAdapter();
    private Context context;
    MyTimerTask myTask = new MyTimerTask();
    Timer myTimer = new Timer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_message);
        chat = (Chat) getIntent().getSerializableExtra("CHAT");
        messageList = chat.getMessages();
        context = this;
        myTimer.schedule(myTask, 5000, 1500);

        mMessageRecycler = (RecyclerView) findViewById(R.id.reyclerview_message_list);
        sendMessage = (Button) findViewById(R.id.button_chatbox_send);
        sendMessage.setOnClickListener(this);
        messageEditText = (EditText) findViewById(R.id.edittext_chatbox);
        mMessageAdapter = new MessageListAdapter(this, messageList);
        mMessageRecycler.setLayoutManager(new LinearLayoutManager(this));
        mMessageRecycler.setAdapter(mMessageAdapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        myTimer.cancel();
        this.finish();
    }

    class MyTimerTask extends TimerTask {
        public void run() {
            refresh();
        }
    }

    @Override
    public void onClick(View view) {
        String message = "";
        message = messageEditText.getText().toString().trim();
        Call<Chat> call = RetrofitClient
                .getInstance()
                .getUserClientRegistration()
                .writeNewMessage(token, new NewMessage(message, chat.getParticipant().getId()));
        call.enqueue(new Callback<Chat>() {
            @Override
            public void onResponse(Call<Chat> call, Response<Chat> response) {
                String s = null;
                try {
                    if (!response.isSuccessful()) {
                        s = response.errorBody().string();
                    } else {
                        mMessageAdapter.notifyDataSetChanged();
                        messageEditText.setText("");
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Chat> call, Throwable t) {
                System.out.println(":(");
            }
        });
    }

    public void refresh() {
        long personId = chat.getId();
        Call<Chat> call = RetrofitClient
                .getInstance()
                .getUserClient()
                .getOneChat(token, chat.getParticipant().getId());
        call.enqueue(new Callback<Chat>() {
            @Override
            public void onResponse(Call<Chat> call, Response<Chat> response) {
                String s = null;
                try {
                    if (!response.isSuccessful()) {
                        s = response.errorBody().string();
                    } else {
                        if (response.body() != null) {
                            messageList = response.body().getMessages();
                            mMessageAdapter = new MessageListAdapter(context, messageList);
                            mMessageRecycler.setAdapter(mMessageAdapter);
                            mMessageAdapter.notifyDataSetChanged();
                        }
                    }
                    System.out.println(response.code());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Chat> call, Throwable t) {
                System.out.println(t.getMessage());
                System.out.println(":(");
            }
        });
    }

}

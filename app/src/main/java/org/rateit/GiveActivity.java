package org.rateit;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rateit.R;

import org.rateit.Models.User;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;

public class GiveActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private List<User> users;
    private TextView noFriends;
    private GiveAdapter giveAdapter;
    int spanCount = 3; // 3 columns
    int spacing = 50; // 50px
    boolean includeEdge = false;
    private long cdId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.relative_give_layout);
        cdId = (long) getIntent().getSerializableExtra("Id");

        recyclerView = findViewById(R.id.recyclerThree);
        noFriends = findViewById(R.id.empty_view_three);
        recyclerView.setLayoutManager(new GridLayoutManager(this, spanCount, RecyclerView.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        myFriendsResponse();

    }


    public void myFriendsResponse() {
        Call<List<User>> call = RetrofitClient
                .getInstance().getUserClient().getMyFriends(token);
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (!response.isSuccessful()) {
//                    textViewResult.setText("Code: " + response.code());
                    System.out.println("Code: " + response.code());
                    return;
                } else {
                    if (users == null) {
                        users = new ArrayList<User>();
                        users = response.body();
                    } else {
                        users = response.body();
                    }
                    if (!users.isEmpty()) {
                        noFriends.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        giveAdapter = new GiveAdapter(users, GiveActivity.this, cdId);
                        recyclerView.setAdapter(giveAdapter);
                        giveAdapter.notifyDataSetChanged();
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        noFriends.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
                System.out.println("NIE poszlo");
                System.out.println(t.getMessage());
            }
        });
    }
}

package org.rateit;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.rateit.R;
import com.squareup.picasso.Picasso;

import org.rateit.Models.CD;
import org.rateit.Models.CDResponse;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;

public class RankingFragment extends Fragment {
    public RatingBar ratingBar;
    private List<CD> cds;
    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_view, container, false);
//        addListenerOnRatingBar(view);
        responseCDCall();
        listView = (ListView) view.findViewById(R.id.listView);
        return view;
    }

    public List<CD> responseCDCall() {
        Call<List<CD>> call = RetrofitClient
                .getInstance().getUserClient().getRankingCD(token, "cds");

        call.enqueue(new Callback<List<CD>>() {
            @Override
            public void onResponse(Call<List<CD>> call, Response<List<CD>> response) {
                if (!response.isSuccessful()) {
//                    textViewResult.setText("Code: " + response.code());
                    System.out.println("Code: " + response.code());
                    return;
                } else {
                    cds = response.body();
                    CustomAdapter customAdapter = new CustomAdapter(cds);
                    listView.setAdapter(customAdapter);
                    //System.out.println(cds.toString());
                }
            }

            @Override
            public void onFailure(Call<List<CD>> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
                System.out.println("NIE poszlo");
                System.out.println(t.getMessage());
            }
        });
        return cds;
    }

    public class CustomAdapter extends BaseAdapter {
        private List<CD> list;

        public CustomAdapter(List<CD> list) {
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            ViewHolder holder;
            convertView = getLayoutInflater().inflate(R.layout.fragment_ranking, null);
            holder = new ViewHolder();


            holder.artistTextView = (TextView) convertView.findViewById(R.id.artistName);
            holder.releasedTextView = (TextView) convertView.findViewById(R.id.released_date);
            holder.imageView = (ImageView) convertView.findViewById(R.id.CDImage);
            holder.ratingBarView = (RatingBar) convertView.findViewById(R.id.ratingBar);
            holder.rankingNumber = (TextView) convertView.findViewById(R.id.rankingNumber);
            holder.detailsButton = (Button) convertView.findViewById(R.id.detailsButton);

            holder.releasedTextView.setText("" + cds.get(i).getReleased());
            Picasso.get().load(cds.get(i).getPhotoURL()).into(holder.imageView);
            holder.ratingBarView.setRating(cds.get(i).getRating());
            holder.artistTextView.setText(cds.get(i).getName());
            holder.rankingNumber.setText("" + (i + 1) + ".");
            holder.detailsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    long cdId = list.get(i).getId();
                    Call<CDResponse> call = RetrofitClient
                            .getInstance()
                            .getUserClient()
                            .getDataCDById(token, "cds", cdId);
                    call.enqueue(new Callback<CDResponse>() {
                        @Override
                        public void onResponse(Call<CDResponse> call, Response<CDResponse> response) {
                            String s = null;
                            try {
                                if (!response.isSuccessful()) {
                                    s = response.errorBody().string();
                                }
                                CDResponse cd = response.body();
                                detailsActivityPage(cd);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<CDResponse> call, Throwable t) {
                            System.out.println(":(");
                        }
                    });
                }
            });


            convertView.setTag(holder);
            return convertView;
        }
    }

    static class ViewHolder {
        Button detailsButton;
        TextView artistTextView, releasedTextView, rankingNumber;
        ImageView imageView;
        RatingBar ratingBarView;

    }

    public void detailsActivityPage(CDResponse cd) {
        Intent intent = new Intent(getActivity(), CDDetailsActivity.class);
        intent.putExtra("CD", cd);
        startActivity(intent);
    }
}
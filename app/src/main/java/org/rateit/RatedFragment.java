package org.rateit;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.rateit.R;
import com.squareup.picasso.Picasso;

import org.rateit.Models.CD;
import org.rateit.Models.CDResponse;
import org.rateit.Models.RatedCD;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;

public class RatedFragment extends Fragment {
    public RatingBar ratingBar;
    private ListView listView;
    private List<CD> cds;
    private float note;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_view, container, false);
        //addListenerOnRatingBar(view);
        responseCDCall();
        listView = (ListView) view.findViewById(R.id.listView);
        return view;
    }

    public void responseCDCall() {
        Call<List<RatedCD>> call = RetrofitClient
                .getInstance().getUserClient().getRatedCD(token, "users", LoginActivity.id);

        call.enqueue(new Callback<List<RatedCD>>() {
            @Override
            public void onResponse(Call<List<RatedCD>> call, Response<List<RatedCD>> response) {
                if (!response.isSuccessful()) {
//                    textViewResult.setText("Code: " + response.code());
                    System.out.println("Code: " + response.code());
                    return;
                } else {
                    List<RatedCD> ratedCDS = response.body();
                    CustomAdapter customAdapter = new CustomAdapter(ratedCDS);
                    listView.setAdapter(customAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<RatedCD>> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
                System.out.println("NIE poszlo");
                System.out.println(t.getMessage());
            }
        });
    }

    public class CustomAdapter extends BaseAdapter {
        private List<RatedCD> list;
        private float note;

        public CustomAdapter(List<RatedCD> list) {
            this.list = list;
        }

        @Override
        public int getCount() {
            //System.out.println(list.toString());
            if (list != null) {
                return list.size();
            } else
                return 0;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            ViewHolder holder;
            convertView = getLayoutInflater().inflate(R.layout.fragment_rated, null);
            holder = new ViewHolder();


            holder.artistTextView = (TextView) convertView.findViewById(R.id.artistName);
            holder.cdNameTextView = (TextView) convertView.findViewById(R.id.CDName);
            holder.imageView = (ImageView) convertView.findViewById(R.id.CDImage);
            holder.ratingBarView = (RatingBar) convertView.findViewById(R.id.ratingBar);
            holder.detailsButton = (Button) convertView.findViewById(R.id.detailsButton);

            holder.cdNameTextView.setText(list.get(i).getCd().getName());
            Picasso.get().load(list.get(i).getCd().getPhotoURL()).into(holder.imageView);
            holder.ratingBarView.setRating(list.get(i).getNote());
            holder.artistTextView.setText(list.get(i).getCd().getArtist());
            holder.detailsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    long cdId = list.get(i).getCd().getId();
                    Call<CDResponse> call = RetrofitClient
                            .getInstance()
                            .getUserClient()
                            .getDataCDById(token, "cds", cdId);
                    call.enqueue(new Callback<CDResponse>() {
                        @Override
                        public void onResponse(Call<CDResponse> call, Response<CDResponse> response) {
                            String s = null;
                            try {
                                if (!response.isSuccessful()) {
                                    s = response.errorBody().string();
                                }
                                CDResponse cd = response.body();
                                detailsActivityPage(cd);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<CDResponse> call, Throwable t) {
                            System.out.println(":(");
                        }
                    });
                }
            });

            convertView.setTag(holder);
            return convertView;
        }
    }

    static class ViewHolder {
        Button detailsButton;
        TextView artistTextView, cdNameTextView;
        ImageView imageView;
        RatingBar ratingBarView;

    }

    public void detailsActivityPage(CDResponse cd) {
        Intent intent = new Intent(getActivity(), CDDetailsActivity.class);
        intent.putExtra("CD", cd);
        startActivity(intent);
    }
}
package org.rateit;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rateit.R;

import org.rateit.Models.Chat;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;

public class MessengerFragment extends Fragment {
    private RecyclerView recyclerView;
    private List<Chat> chats;
    private MessengerAdapter messengerAdapter;
    private ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.relative_view, container, false);
        progressBar = view.findViewById(R.id.progress_searching);
        recyclerView = view.findViewById(R.id.recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        chatResponse();

        return view;
    }

    public void chatResponse() {
        Call<List<Chat>> call = RetrofitClient
                .getInstance().getUserClient().getMyChats(token);

        call.enqueue(new Callback<List<Chat>>() {
            @Override
            public void onResponse(Call<List<Chat>> call, Response<List<Chat>> response) {
                if (!response.isSuccessful()) {
//                    textViewResult.setText("Code: " + response.code());
                    System.out.println("Code: " + response.code());
                    return;
                } else {
                    progressBar.setVisibility(View.GONE);
                    if (response.body() != null) {
                        chats = response.body();
                        messengerAdapter = new MessengerAdapter(chats, getActivity());
                        recyclerView.setAdapter(messengerAdapter);
                        //System.out.println(cds.toString());
                    } else {
                        return;
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Chat>> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
                System.out.println("NIE poszlo");
                System.out.println(t.getMessage());
            }
        });
    }

}
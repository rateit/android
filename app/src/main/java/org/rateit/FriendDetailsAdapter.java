package org.rateit;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rateit.R;
import com.facebook.drawee.view.SimpleDraweeView;

import org.rateit.Models.OwnedAlbumMap;

import java.util.List;

public class FriendDetailsAdapter extends RecyclerView.Adapter<FriendDetailsAdapter.MyViewHolder>{

    private android.content.Context context;
    private List<OwnedAlbumMap> list;



    public FriendDetailsAdapter() {
    }

    public FriendDetailsAdapter(List<OwnedAlbumMap> list, Context context) {
        this.list = list;
        this.context = context;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cd_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.cdName.setText(list.get(position).getCd().getName());
        holder.artistName.setText(list.get(position).getCd().getArtist());
        holder.cdType.setText(list.get(position).getCdType().getType());
        holder.imageView.setImageURI(Uri.parse(list.get(position).getCd().getPhotoURL()));
        System.out.println(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView cdName, artistName, cdType;
        SimpleDraweeView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            cdName = (TextView) itemView.findViewById(R.id.cdName);
            artistName = (TextView) itemView.findViewById(R.id.artistName);
            cdType = (TextView) itemView.findViewById(R.id.cdType);
            imageView = (SimpleDraweeView) itemView.findViewById(R.id.CDImage);
        }
    }
}

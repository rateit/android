package org.rateit;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rateit.R;
import com.facebook.drawee.view.SimpleDraweeView;

import org.rateit.Models.OwnedAlbumMap;
import org.rateit.Models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;

public class FriendsDetailsActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private List<User> users;
    private TextView noFriends;
    private FriendDetailsAdapter friendsDetailsAdapter;
    int spanCount = 3; // 3 columns
    int spacing = 50; // 50px
    boolean includeEdge = false;
    private TextView username;
    private TextView email;
    private SimpleDraweeView imageView;
    private long cdId;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = (User) getIntent().getSerializableExtra("USER");
        setContentView(R.layout.friend_details);
        username = (TextView) findViewById(R.id.username);
        email = (TextView) findViewById(R.id.email);
        imageView = (SimpleDraweeView) findViewById(R.id.CDImage);
        username.setText(user.getUsername());
        email.setText(user.getEmail());
        imageView.setImageURI(Uri.parse(user.getPhotoURL()));
        recyclerView = findViewById(R.id.userscdlist);
        noFriends = findViewById(R.id.empty_view_three);
        recyclerView.setLayoutManager(new GridLayoutManager(this, spanCount, RecyclerView.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        myFriendsResponse();

    }
    public void myFriendsResponse() {
        Call<List<OwnedAlbumMap>> call = RetrofitClient
                .getInstance().getUserClient().getUserCd(token, user.getId());
        call.enqueue(new Callback<List<OwnedAlbumMap>>() {
            @Override
            public void onResponse(Call<List<OwnedAlbumMap>> call, Response<List<OwnedAlbumMap>> response) {
                if (!response.isSuccessful()) {
//                    textViewResult.setText("Code: " + response.code());
                    System.out.println("Code: " + response.code());
                    return;
                } else {
                    if(response.body() == null || response.body().isEmpty()){
                        recyclerView.setVisibility(View.GONE);
                        noFriends.setVisibility(View.VISIBLE);
                    }else{
                        noFriends.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        friendsDetailsAdapter = new FriendDetailsAdapter(response.body(), FriendsDetailsActivity.this);
                        recyclerView.setAdapter(friendsDetailsAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<OwnedAlbumMap>> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
                System.out.println("NIE poszlo");
                System.out.println(t.getMessage());
            }
        });
    }

}

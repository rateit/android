package org.rateit;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rateit.R;
import com.facebook.drawee.view.SimpleDraweeView;

import org.rateit.Models.OwnedAlbumMap;
import org.rateit.Models.User;
import org.rateit.Models.Wishlist;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;

public class GiveAdapter extends RecyclerView.Adapter<GiveAdapter.MyViewHolder> {

    private List<User> list;
    private android.content.Context context;
    private long cdId;
    private List<Wishlist> wishlists;



    public GiveAdapter() {
    }

    public GiveAdapter(List<User> list, Context context, long cdId) {
        this.list = list;
        this.context = context;
        this.cdId = cdId;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_to_give_activity, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.usernameTextView.setText("" + list.get(position).getUsername());
        holder.imageView.setImageURI(Uri.parse(list.get(position).getPhotoURL()));
        holder.giveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Call<OwnedAlbumMap> call = RetrofitClient
                        .getInstance()
                        .getUserClient()
                        .giveCD(token, list.get(position).getId(), cdId);
                call.enqueue(new Callback<OwnedAlbumMap>() {
                    @Override
                    public void onResponse(Call<OwnedAlbumMap> call, Response<OwnedAlbumMap> response) {
                        String s = null;
                        try {
                            if (!response.isSuccessful()) {
                                s = response.errorBody().string();
                            }else{
                                Toast.makeText(context, "Give to friend succesfully", Toast.LENGTH_LONG).show();
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<OwnedAlbumMap> call, Throwable t) {
                        System.out.println(":(");
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        Button giveButton;
        TextView usernameTextView;
        SimpleDraweeView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            giveButton = itemView.findViewById(R.id.give_cd);
            usernameTextView = itemView.findViewById(R.id.proposal_friend_nick);
            imageView = (SimpleDraweeView) itemView.findViewById(R.id.friend_avatar);
        }
    }
}
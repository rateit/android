package org.rateit;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rateit.R;
import com.squareup.picasso.Picasso;

import org.rateit.Models.Chat;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;

public class MessengerAdapter extends RecyclerView.Adapter<MessengerAdapter.MyViewHolder> {

    private Context context;
    private List<Chat> chats;


    public MessengerAdapter() {
    }

    public MessengerAdapter(List<Chat> chats, Context context) {
        this.chats = chats;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_messenger, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        System.out.println(chats.toString());
        //System.out.println(chats.get(position).getMessages().toString());
        holder.button.setId(position);
        holder.recipientTextView.setText("" + chats.get(position).getParticipant().getUsername());
        Picasso.get().load(chats.get(position).getParticipant().getPhotoURL()).into(holder.imageView);
        //holder.lastMessageTextView.setText(chats.get(position).getMessages().get(chats.get(position).getMessages().size() - 1).getMessage());
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long personId = chats.get(position).getId();
                Call<Chat> call = RetrofitClient
                        .getInstance()
                        .getUserClient()
                        .getOneChat(token, chats.get(position).getParticipant().getId());
                call.enqueue(new Callback<Chat>() {
                    @Override
                    public void onResponse(Call<Chat> call, Response<Chat> response) {
                        String s = null;
                        try {
                            if (!response.isSuccessful()) {
                                s = response.errorBody().string();
                            } else {
                                if (response.body() != null)
                                    messagesActivityPage(response.body());
                            }
                            System.out.println(response.code());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<Chat> call, Throwable t) {
                        System.out.println(t.getMessage());
                        System.out.println(":(");
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        Button button;
        TextView recipientTextView;
        ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            button = itemView.findViewById(R.id.messages);
            recipientTextView = itemView.findViewById(R.id.recipient);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }

    public void messagesActivityPage(Chat chat) {
        Intent intent = new Intent(context, MessagingActivity.class);
        intent.putExtra("CHAT", chat);
        context.startActivity(intent);
    }
}

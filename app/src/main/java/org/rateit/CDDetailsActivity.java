package org.rateit;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rateit.R;
import com.facebook.drawee.backends.pipeline.Fresco;

import org.rateit.Models.CDResponse;
import org.rateit.Models.Comments;
import org.rateit.Models.Rate;
import org.rateit.Models.Track;
import org.rateit.Models.User;
import org.rateit.Models.Wishlist;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;

public class CDDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    private String name;
    private int released;
    private float rating = 0;
    private String photoURL = "http://bobjames.com/wp-content/themes/soundcheck/images/default-album-artwork.png";
    private String artist;
    private List<Track> cdtracks;
    private String genre;
    private List<Comments> comments;
    private TextView artistName;
    private TextView cdName;
    private TextView releaseDate;
    private TextView ratingText;
    private RatingBar ratingBar;
    private TextView cdTracks;
    private TextView commentsView;
    private ImageView imageView;
    private EditText commentEditText;
    private long cdId;
    private CommentsAdapter commentsAdapter;
    private TrackAdapter trackAdapter;
    private List<Comments> commentsList;
    private CDResponse cd;
    private RecyclerView recyclerViewForComments;
    private RecyclerView recyclerViewForTracks;
    Button sendCommentButton;
    Button addToWishlist;
    Button addToOwned;
    private String cdType;

    public CDDetailsActivity() {
    }

    public CDDetailsActivity(String name, List<Comments> comments, int released, float rating, String photoURL, String artist, List<Track> cdtracks, String genre) {
        this.name = name;
        this.comments = comments;
        this.released = released;
        this.rating = rating;
        this.photoURL = photoURL;
        this.artist = artist;
        this.cdtracks = cdtracks;
        this.genre = genre;
    }

    private AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int position, long id) {
            Log.v("item", (String) parent.getItemAtPosition(position));
            cdType = parent.getItemAtPosition(position).toString();
            System.out.println(cdType);
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_details);
        cd = (CDResponse) getIntent().getSerializableExtra("CD");
        cdId = cd.getCd().getId();

        findViewById(R.id.send_comment_button).setOnClickListener(this);
        findViewById(R.id.addToWishlistButton).setOnClickListener(this);
        findViewById(R.id.addToOwnedButton).setOnClickListener(this);
        Spinner dropdown = findViewById(R.id.spinner);
        String[] items = new String[]{"vinyl", "cd", "cassette", "other"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);
        dropdown.setOnItemSelectedListener(itemSelectedListener);
        artistName = (TextView) findViewById(R.id.artistName);
        releaseDate = (TextView) findViewById(R.id.released_date);
        cdName = (TextView) findViewById(R.id.CDName);
        imageView = (ImageView) findViewById(R.id.CDImage);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        commentEditText = (EditText) findViewById(R.id.commentEditText);
        recyclerViewForComments = findViewById(R.id.comment_list);
        LinearLayoutManager layoutManagerForComments = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewForComments.setLayoutManager(layoutManagerForComments);
        recyclerViewForComments.setHasFixedSize(true);
        if (cd.getComments() != null) {
            commentsAdapter = new CommentsAdapter(cd.getComments());
            recyclerViewForComments.setAdapter(commentsAdapter);
        }
        recyclerViewForTracks = findViewById(R.id.track_list);
        LinearLayoutManager layoutManagerForTracks = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewForTracks.setLayoutManager(layoutManagerForTracks);
        recyclerViewForTracks.setHasFixedSize(true);
        if (cd.getCd().getCdtracks() != null) {
            trackAdapter = new TrackAdapter(cd.getCd().getCdtracks());
            recyclerViewForTracks.setAdapter(trackAdapter);
        }
        recyclerViewForTracks.setNestedScrollingEnabled(false);
        recyclerViewForComments.setNestedScrollingEnabled(false);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                sendRate();
            }
        });


        artistName.setText(cd.getCd().getArtist());
        releaseDate.setText("" + cd.getCd().getReleased());
        cdName.setText(cd.getCd().getName());
        imageView.setImageURI(Uri.parse(cd.getCd().getPhotoURL()));


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.send_comment_button:
                sendComment(view);
                break;
            case R.id.addToOwnedButton:
                addToOwned(view);
                break;
            case R.id.addToWishlistButton:
                addToWishlist(view);
                break;
        }
    }
    private void addToOwned(View v){
        Call<User> call = RetrofitClient
                .getInstance()
                .getUserClientRegistration()
                .postOwnedCD(token, "users", LoginActivity.id, cdId, LoginActivity.user, cdType);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                String s = null;
                try {
                    if (!response.isSuccessful()) {
                        s = response.errorBody().string();
                        Toast.makeText(CDDetailsActivity.this, s, Toast.LENGTH_LONG).show();
                    }
                    System.out.println(response.code());

                    Toast.makeText(CDDetailsActivity.this, "Add to owned succesfully", Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(CDDetailsActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                System.out.println(":(");
            }
        });
    }
    private void addToWishlist(View v){
        Call<Wishlist> call = RetrofitClient
                .getInstance()
                .getUserClientRegistration()
                .addToWishlist(token, cdId, cdType);
        call.enqueue(new Callback<Wishlist>() {
            @Override
            public void onResponse(Call<Wishlist> call, Response<Wishlist> response) {
                String s = null;
                try {
                    if (!response.isSuccessful()) {
                        s = response.errorBody().string();
                        Toast.makeText(CDDetailsActivity.this, "You can't add the same carrier to collection", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(CDDetailsActivity.this, "Add to Wishlist succesfully", Toast.LENGTH_LONG).show();
                    }
                    System.out.println(response.code());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Wishlist> call, Throwable t) {
                Toast.makeText(CDDetailsActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                System.out.println(":(");
            }
        });
    }

    private void sendComment(View v) {
        String commentTextView = commentEditText.getText().toString().trim();
        Call<Comments> call = RetrofitClient
                .getInstance()
                .getUserClientRegistration()
                .addComment(token, new Comments(commentTextView, LoginActivity.id), cdId);
        call.enqueue(new Callback<Comments>() {
            @Override
            public void onResponse(Call<Comments> call, Response<Comments> response) {
                String s = null;
                try {
                    if (!response.isSuccessful()) {
                        s = response.errorBody().string();
                        Toast.makeText(CDDetailsActivity.this, s, Toast.LENGTH_LONG).show();
                    } else {
                        commentEditText.setText("");
                        getComment();
                    }
                    System.out.println(response.code());

                    Toast.makeText(CDDetailsActivity.this, "Comment add succefully", Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Comments> call, Throwable t) {
                Toast.makeText(CDDetailsActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                System.out.println(":(");
            }
        });
    }

    private void getComment() {
        Call<CDResponse> call = RetrofitClient
                .getInstance()
                .getUserClientRegistration()
                .getDataCDById(token, "cds", cdId);
        call.enqueue(new Callback<CDResponse>() {
            @Override
            public void onResponse(Call<CDResponse> call, Response<CDResponse> response) {
                String s = null;
                try {
                    if (!response.isSuccessful()) {
                        s = response.errorBody().string();
                        Toast.makeText(CDDetailsActivity.this, s, Toast.LENGTH_LONG).show();
                    }
                    System.out.println(response.body().toString());
                    if (response.body().getComments() != null) {
                        commentsAdapter = new CommentsAdapter(response.body().getComments());
                        recyclerViewForComments.setAdapter(commentsAdapter);
                        commentsAdapter.notifyDataSetChanged();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CDResponse> call, Throwable t) {
                Toast.makeText(CDDetailsActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                System.out.println(":(");
            }
        });
    }

    private void sendRate() {
        float ratingValue = ratingBar.getRating();
        cd.getCd().setRating(ratingValue);
        Call<Rate> call = RetrofitClient
                .getInstance()
                .getUserClientRegistration()
                .addRate(token, cdId, ratingValue, LoginActivity.id);
        call.enqueue(new Callback<Rate>() {
            @Override
            public void onResponse(Call<Rate> call, Response<Rate> response) {
                String s = null;
                try {
                    if (!response.isSuccessful()) {
                        s = response.errorBody().string();
                        Toast.makeText(CDDetailsActivity.this, s, Toast.LENGTH_LONG).show();
                    }
                    System.out.println(response.code());

                    Toast.makeText(CDDetailsActivity.this, "Your rate: " + ratingValue, Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Rate> call, Throwable t) {
                Toast.makeText(CDDetailsActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                System.out.println(":(");
            }
        });
    }
}
//serializacja
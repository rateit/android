package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class UserWishlist implements Serializable {
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("wishList")
    @Expose
    private List<Wishlist> wishlists;

    public UserWishlist(long id, User user, List<Wishlist> wishlists) {
        this.id = id;
        this.user = user;
        this.wishlists = wishlists;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Wishlist> getWishlists() {
        return wishlists;
    }

    public void setWishlists(List<Wishlist> wishlists) {
        this.wishlists = wishlists;
    }

    @Override
    public String toString() {
        return "UserWishlist{" +
                "id=" + id +
                ", user=" + user +
                ", wishlists=" + wishlists +
                '}';
    }
}

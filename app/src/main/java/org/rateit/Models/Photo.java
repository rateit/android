package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Photo {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("photoURL")
    @Expose
    private String photoURL;
}

package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CDType {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("cds")
    @Expose
    private List<OwnedAlbumMap> ownedAlbumMaps;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<OwnedAlbumMap> getOwnedAlbumMaps() {
        return ownedAlbumMaps;
    }

    public void setOwnedAlbumMaps(List<OwnedAlbumMap> ownedAlbumMaps) {
        this.ownedAlbumMaps = ownedAlbumMaps;
    }

    @Override
    public String toString() {
        return "CDType{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", ownedAlbumMaps=" + ownedAlbumMaps +
                '}';
    }
}

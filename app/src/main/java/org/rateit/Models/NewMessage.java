package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewMessage {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("receiverId")
    @Expose
    private long receiverId;

    public NewMessage(String message, long receiverId) {
        this.message = message;
        this.receiverId = receiverId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(int receiverId) {
        this.receiverId = receiverId;
    }

    @Override
    public String toString() {
        return "NewMessage{" +
                "message='" + message + '\'' +
                ", receiverId=" + receiverId +
                '}';
    }
}

package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserProposition implements Serializable {
    @SerializedName("howManyMutualFriends")
    @Expose
    private int howManyMutualFriends;
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("nick")
    @Expose
    private String nick;
    @SerializedName("photoURL")
    @Expose
    private String photoURL;

    public UserProposition(int howManyMutualFriends, long id, String nick, String photoURL) {
        this.howManyMutualFriends = howManyMutualFriends;
        this.id = id;
        this.nick = nick;
        this.photoURL = photoURL;
    }

    public int getHowManyMutualFriends() {
        return howManyMutualFriends;
    }

    public void setHowManyMutualFriends(int howManyMutualFriends) {
        this.howManyMutualFriends = howManyMutualFriends;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    @Override
    public String toString() {
        return "UserProposition{" +
                "howManyMutualFriends=" + howManyMutualFriends +
                ", id=" + id +
                ", nick='" + nick + '\'' +
                ", photoURL='" + photoURL + '\'' +
                '}';
    }
}

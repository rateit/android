package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class User implements Serializable {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("nick")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("confirmPassword")
    @Expose
    private String confirmPassword;
    @SerializedName("score")
    @Expose
    private int score;
    @SerializedName("badges")
    @Expose
    private String badges;
    @SerializedName("registrationDate")
    @Expose
    private Long registrationDate;
    @SerializedName("isActive")
    @Expose
    private boolean isActive;
    @SerializedName("photoURL")
    @Expose
    private String photoURL;
    @SerializedName("userscd")
    @Expose
    private List<OwnedAlbumMap> userscd;
    private String role;
    @SerializedName("comments")
    @Expose
    private List<Comments> comments;
    @SerializedName("authToken")
    @Expose
    private String authToken;

    public User(String confirmPassword, String email, String password, String username) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.confirmPassword = confirmPassword;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getBadges() {
        return badges;
    }

    public void setBadges(String badges) {
        this.badges = badges;
    }

    public Long getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Long registrationDate) {
        this.registrationDate = registrationDate;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public List<OwnedAlbumMap> getUserscd() {
        return userscd;
    }

    public void setUserscd(OwnedAlbumMap ownedAlbumMap) {
        if (userscd == null)
            userscd = new ArrayList<OwnedAlbumMap>();
        else
            this.userscd.add(ownedAlbumMap);
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<Comments> getComments() {
        return comments;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", confirmPassword='" + confirmPassword + '\'' +
                ", score=" + score +
                ", badges='" + badges + '\'' +
                ", registrationDate=" + registrationDate +
                ", isActive=" + isActive +
                ", photoURL='" + photoURL + '\'' +
                ", userscd=" + userscd +
                ", role='" + role + '\'' +
                ", comments=" + comments +
                ", authToken='" + authToken + '\'' +
                '}';
    }
}

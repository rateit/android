package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangeEmail {
    @SerializedName("confirmEmail")
    @Expose
    private String confirmEmail;
    @SerializedName("newEmail")
    @Expose
    private String newEmail;
    @SerializedName("oldEmail")
    @Expose
    private String oldEmail;

    public ChangeEmail(String confirmEmail, String newEmail, String oldEmail) {
        this.confirmEmail = confirmEmail;
        this.newEmail = newEmail;
        this.oldEmail = oldEmail;
    }
}

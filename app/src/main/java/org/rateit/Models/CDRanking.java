package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CDRanking implements Serializable {
    @SerializedName("cds")
    @Expose
    private List<CD> cds;

    public List<CD> getCds() {
        return cds;
    }


    @Override
    public String toString() {
        return "CDAllResponse{" +
                "cds=" + cds +
                '}';
    }
}

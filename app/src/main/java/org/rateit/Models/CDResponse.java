package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CDResponse implements Serializable {
    @SerializedName("cd")
    @Expose
    private CD cd;
    @SerializedName("comments")
    @Expose
    private List<Comments> comments;

    public CD getCd() {
        return cd;
    }

    public List<Comments> getComments() {
        return comments;
    }

    public void setCd(CD cd) {
        this.cd = cd;
    }

    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return cd + " " + comments;
    }
}

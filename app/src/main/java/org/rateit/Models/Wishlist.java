package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Wishlist {
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("friendsAlbums")
    @Expose
    private List<OwnedAlbumMap> friendsAlbums;
    @SerializedName("cdType")
    @Expose
    private CDType cdType;
    @SerializedName("cd")
    @Expose
    private CD cd;

    public Wishlist(long id, List<OwnedAlbumMap> friendsAlbums, CDType cdType, CD cd) {
        this.id = id;
        this.friendsAlbums = friendsAlbums;
        this.cdType = cdType;
        this.cd = cd;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<OwnedAlbumMap> getFriendsAlbums() {
        return friendsAlbums;
    }

    public void setFriendsAlbums(List<OwnedAlbumMap> friendsAlbums) {
        this.friendsAlbums = friendsAlbums;
    }

    public CDType getCdType() {
        return cdType;
    }

    public void setCdType(CDType cdType) {
        this.cdType = cdType;
    }

    public CD getCd() {
        return cd;
    }

    public void setCd(CD cd) {
        this.cd = cd;
    }

    @Override
    public String toString() {
        return "Wishlist{" +
                "id=" + id +
                ", friendsAlbums=" + friendsAlbums +
                ", cdType=" + cdType +
                ", cd=" + cd +
                '}';
    }
}

package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Chat implements Serializable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("me")
    @Expose
    private User me;
    @SerializedName("messages")
    @Expose
    private List<Message> messages;
    @SerializedName("participant")
    @Expose
    private User participant;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getMe() {
        return me;
    }

    public void setMe(User me) {
        this.me = me;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public User getParticipant() {
        return participant;
    }

    public void setParticipant(User participant) {
        this.participant = participant;
    }

    public Chat(int id, User me, List<Message> messages, User participant) {
        this.id = id;
        this.me = me;
        this.messages = messages;
        this.participant = participant;
    }

    public Chat(User me, List<Message> messages, User participant) {
        this.me = me;
        this.messages = messages;
        this.participant = participant;
    }

    @Override
    public String toString() {
        return "Chat{" +
                "id=" + id +
                ", me=" + me +
                ", messages=" + messages +
                ", participant=" + participant +
                '}';
    }
}

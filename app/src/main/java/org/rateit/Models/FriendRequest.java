package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FriendRequest {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("requesterId")
    @Expose
    private long requesterId;
    @SerializedName("receiverId")
    @Expose
    private long receiverId;
    @SerializedName("requester")
    @Expose
    private User requester;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("seen")
    @Expose
    private boolean seen;

    public FriendRequest() {
    }

    public FriendRequest(long requesterId, long receiverId) {
        this.receiverId = receiverId;
        this.requesterId = requesterId;
    }

    public FriendRequest(User requester, String status, boolean seen) {
        this.requester = requester;
        this.status = status;
        this.seen = seen;
    }

    public User getRequester() {
        return requester;
    }

    public void setRequester(User requester) {
        this.requester = requester;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRequesterId() {
        return requesterId;
    }

    public void setRequesterId(long requesterId) {
        this.requesterId = requesterId;
    }

    public long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(long receiverId) {
        this.receiverId = receiverId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    @Override
    public String toString() {
        return "FriendRequest{" +
                "id=" + id +
                ", requesterId=" + requesterId +
                ", receiverId=" + receiverId +
                ", requester=" + requester +
                ", status='" + status + '\'' +
                ", seen=" + seen +
                '}';
    }
}

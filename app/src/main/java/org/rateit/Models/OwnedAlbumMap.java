package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OwnedAlbumMap {
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("cd")
    @Expose
    private CD cd;
    @SerializedName("photos")
    @Expose
    private List<Photo> photos;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("cdType")
    @Expose
    private CDType cdType;

    public OwnedAlbumMap(CD cd, List<Photo> photos, User user, CDType cdType) {
        this.cd = cd;
        this.photos = photos;
        this.user = user;
        this.cdType = cdType;
    }

    public long getId() {
        return id;
    }

    public CD getCd() {
        return cd;
    }

    public List<Photo> getPhoto() {
        return photos;
    }

    public void setCD(CD cd) {
        this.cd = cd;
    }

    public CDType getCdType() {
        return cdType;
    }

    public void setCdType(CDType cdType) {
        this.cdType = cdType;
    }

    @Override
    public String toString() {
        return "[id=" + id +
                "cd=" + cd +
                ", photos=" + photos +
                '}';
    }
}

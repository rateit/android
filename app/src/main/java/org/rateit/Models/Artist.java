package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Set;

public class Artist {
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("stageName")
    @Expose
    private String stageName;
    @SerializedName("cd")
    @Expose
    private Set<CD> cd;
    @SerializedName("tracks")
    @Expose
    private Set<Track> tracks;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public Set<CD> getCd() {
        return cd;
    }

    public Set<Track> getTrack() {
        return tracks;
    }

    @Override
    public String toString() {
        return "Artist{" +
                "id=" + id +
                ", stageName='" + stageName + '\'' +
                ", cd=" + cd +
                ", tracks=" + tracks +
                '}';
    }
}

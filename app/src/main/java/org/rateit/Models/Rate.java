package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rate {

    private long id;

    @SerializedName("note")
    @Expose
    private float note;
    private long cdId;
    @SerializedName("userId")
    @Expose
    private Long userId;
    private CD cd;

    public Rate(float rating, long cdId, Long userId) {
        this.note = note;
        this.cdId = cdId;
        this.userId = userId;
    }

    public Rate(float note, Long userId) {
        this.note = note;
        this.userId = userId;
    }

    public Rate(CD cd, float note) {
        this.cd = cd;
        this.note = note;
    }

    public Rate(float note) {
        this.note = note;
    }

    public Rate() {
    }

    public float getRating() {
        return note;
    }

    public void setRating(float rating) {
        this.note = note;
    }

    public long getCd() {
        return cdId;
    }

    public void setCd(long cd) {
        this.cdId = cd;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}

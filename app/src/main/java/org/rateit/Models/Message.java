package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Message implements Serializable {
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("senderId")
    @Expose
    private long senderId;
    @SerializedName("seen")
    @Expose
    private boolean seen;
    @SerializedName("sender")
    @Expose
    private User sender;
    @SerializedName("time")
    @Expose
    private long time;


    public Message(long id, String message, long senderId, boolean seen, User sender, long time) {
        this.id = id;
        this.message = message;
        this.senderId = senderId;
        this.seen = seen;
        this.sender = sender;
        this.time = time;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public long getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", message='" + message + '\'' +
                ", seen=" + seen +
                ", sender=" + sender +
                ", time=" + time +
                '}';
    }
}

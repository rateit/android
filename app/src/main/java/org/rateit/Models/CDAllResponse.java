package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CDAllResponse {
    @SerializedName("cds")
    @Expose
    private List<CD> cds;
    @SerializedName("context")
    @Expose
    private Context context;

    public List<CD> getCds() {
        return cds;
    }

    public Context getContext() {
        return context;
    }

    @Override
    public String toString() {
        return "CDAllResponse{" +
                "cds=" + cds +
                ", context=" + context +
                '}';
    }
}

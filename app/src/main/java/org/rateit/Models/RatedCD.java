package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RatedCD implements Serializable {
    @SerializedName("album")
    @Expose
    private CD cd;
    @SerializedName("note")
    @Expose
    private float note;

    public CD getCd() {
        return cd;
    }

    public float getNote() {
        return note;
    }

    @Override
    public String toString() {
        return "RatedCD{" +
                "cd=" + cd +
                ", note=" + note +
                '}';
    }
}

package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CD implements Serializable {
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("released")
    @Expose
    private int released;
    @SerializedName("rating")
    @Expose
    private float rating = 0;
    @SerializedName("photoURL")
    @Expose
    private String photoURL = "http://bobjames.com/wp-content/themes/soundcheck/images/default-album-artwork.png";
    @SerializedName("artist")
    @Expose
    private String artist;
    @SerializedName("cdtracks")
    @Expose
    private List<Track> cdtracks;
    @SerializedName("genre")
    @Expose
    private List<Genre> genre;
    @SerializedName("cdType")
    @Expose
    private CDType type;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getReleased() {
        return released;
    }

    public void setReleased(int released) {
        this.released = released;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public List<Track> getCdtracks() {
        return cdtracks;
    }

    public List<Genre> getGenre() {
        return genre;
    }

    public void setGenre(List<Genre> genre) {
        this.genre = genre;
    }


    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }


    public CDType getType() {
        return type;
    }

    public void setType(CDType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "cd{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", released=" + released +
                ", rating=" + rating +
                ", photoURL='" + photoURL + '\'' +
                ", artist='" + artist + '\'' +
                ", cdtracks=" + cdtracks +
                ", genre='" + genre + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}

package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangePassword {
    @SerializedName("confirmPassword")
    @Expose
    private String confirmPassword;
    @SerializedName("newPassword")
    @Expose
    private String newPassword;
    @SerializedName("oldPassword")
    @Expose
    private String oldPassword;

    public ChangePassword(String confirmPassword, String newPassword, String oldPassword) {
        this.confirmPassword = confirmPassword;
        this.newPassword = newPassword;
        this.oldPassword = oldPassword;
    }
}

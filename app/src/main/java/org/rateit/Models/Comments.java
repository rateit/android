package org.rateit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Comments implements Serializable {

    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("content")
    @Expose
    private String content;


    @SerializedName("user")
    @Expose
    private User user;

    private Long userId;

    public Comments(String content, Long userId) {
        this.content = content;
        this.userId = userId;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }


    @Override
    public String toString() {
        return "Comments{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", user=" + user +
                '}';
    }
}

package org.rateit;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rateit.R;

import org.rateit.Models.Comments;

import java.util.List;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MyViewHolder> {
    private List<Comments> list;


    public CommentsAdapter() {
    }

    public CommentsAdapter(List<Comments> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (list.get(position).getUser().getPhotoURL() != null) {
            holder.commentAvatar.setImageURI(Uri.parse(list.get(position).getUser().getPhotoURL()));
        } else {
            holder.commentAvatar.setImageResource(R.drawable.ic_account);
        }
        holder.commentText.setText(list.get(position).getContent());
        holder.commentUsername.setText(list.get(position).getUser().getUsername());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView commentText, commentUsername;
        ImageView commentAvatar;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            commentText = itemView.findViewById(R.id.commentText);
            commentAvatar = itemView.findViewById(R.id.commentAvatar);
            commentUsername = itemView.findViewById(R.id.commentUsername);
        }
    }
}

package org.rateit;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.rateit.R;
import com.google.android.material.navigation.NavigationView;

import org.rateit.Models.ChangeEmail;
import org.rateit.Models.ChangePassword;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;

public class MyAccountFragment extends Fragment implements View.OnClickListener{
    private EditText currentEmail, newEmail, newEmailConfirm, currentPassword, newPassword, newPasswordConfirm;
    private Button passwordConfirmButton, emailConfirmButton;
    private NavigationView navigationView;
    private View headerView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_account, container, false);
        currentEmail = (EditText) view.findViewById(R.id.currentEmail);
        newEmail = (EditText) view.findViewById(R.id.newEmail);
        newEmailConfirm = (EditText) view.findViewById(R.id.newEmailConfirm);
        currentPassword = (EditText) view.findViewById(R.id.currentPassword);
        newPassword = (EditText) view.findViewById(R.id.newPassword);
        newPasswordConfirm = (EditText) view.findViewById(R.id.newpasswordConfirm);
        passwordConfirmButton = (Button) view.findViewById(R.id.passwordConfirmButton);
        emailConfirmButton = (Button) view.findViewById(R.id.emailConfirmButton);
        passwordConfirmButton.setOnClickListener(this);
        emailConfirmButton.setOnClickListener(this);
        return view;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.emailConfirmButton:
        editEmail(v);
        break;
        case R.id.passwordConfirmButton:
        editPassword(v);
        break;
    }

    }
    boolean isEmail(EditText text) {
        CharSequence email = text.getText().toString();
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    void checkDataEnteredPassword() {
        if (isEmpty(currentPassword)) {
            Toast t = Toast.makeText(getContext(), "You must enter current password to edit!", Toast.LENGTH_SHORT);
            t.show();
        }

        if (isEmpty(newPassword)) {
            newPassword.setError("new password is required!");
        }
        if (isEmpty(newPasswordConfirm)) {
            newPasswordConfirm.setError("new password confirm is required!");
        }
    }
    void checkDataEnteredEmail() {

        if (isEmail(currentEmail) == false) {
            currentEmail.setError("Enter valid current email!");
        }
        if (isEmail(newEmail) == false) {
            newEmail.setError("Enter valid new Email email!");
        }
        if (isEmail(newEmailConfirm) == false) {
            newEmailConfirm.setError("Enter valid new Email Confirm email!");
        }

    }
    public void editEmail(View v){
        String cEmail = currentEmail.getText().toString().trim();
        String nEmail = newEmail.getText().toString().trim();
        String nEmailC = newEmailConfirm.getText().toString().trim();
        Call<ChangeEmail> call = RetrofitClient
                .getInstance().getUserClient().editEmail(token, LoginActivity.id, new ChangeEmail(nEmailC, nEmail, cEmail));

        call.enqueue(new Callback<ChangeEmail>() {
            @Override
            public void onResponse(Call<ChangeEmail> call, Response<ChangeEmail> response) {
                checkDataEnteredEmail();
                if (!response.isSuccessful()) {
//                    textViewResult.setText("Code: " + response.code());
                    Toast.makeText(getActivity(), "Try to pass correct email", Toast.LENGTH_SHORT).show();
                    System.out.println("Code: " + response.code());
                    return;
                } else {
                    newEmail.setText("");
                    currentEmail.setText("");
                    newEmailConfirm.setText("");
                    Toast.makeText(getActivity(), "Email edited succesfully", Toast.LENGTH_LONG).show();
                    }
                }

            @Override
            public void onFailure(Call<ChangeEmail> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
                System.out.println("NIE poszlo");
                System.out.println(t.getMessage());
            }
        });
    }
    public void editPassword(View v){
        String nPassword = newPassword.getText().toString().trim();
        String nPasswordC = newPasswordConfirm.getText().toString().trim();
        String cPassword = currentPassword.getText().toString().trim();
        Call<ChangePassword> call = RetrofitClient
                .getInstance().getUserClient().editPassword(token, LoginActivity.id, new ChangePassword(nPasswordC, nPassword, cPassword));

        call.enqueue(new Callback<ChangePassword>() {
            @Override
            public void onResponse(Call<ChangePassword> call, Response<ChangePassword> response) {
                checkDataEnteredPassword();
                if (!response.isSuccessful()) {
//                    textViewResult.setText("Code: " + response.code());
                    Toast.makeText(getActivity(), "Try to pass correct password", Toast.LENGTH_SHORT).show();
                    System.out.println("Code: " + response.code());
                    return;
                } else {
                    newPassword.setText("");
                    currentPassword.setText("");
                    newPasswordConfirm.setText("");
                    Toast.makeText(getActivity(), "Password edited successfully", Toast.LENGTH_LONG).show();
                    }
                }

            @Override
            public void onFailure(Call<ChangePassword> call, Throwable t) {
                System.out.println("NIE poszlo");
                System.out.println(t.getMessage());
            }
        });
    }
}
package org.rateit;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.rateit.R;

import org.rateit.Models.Login;
import org.rateit.Models.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText passwordEditText;
    private EditText usernameEditText;
    public static String usernameHeader;
    public static String emailHeader;
    public static String imageHeader;
    public static Long id;
    public static User user;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:
                userLogin(v);
                break;
            case R.id.register:
                registrationActivityPage(v);
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameEditText = (EditText) findViewById(R.id.username);
        passwordEditText = (EditText) findViewById(R.id.password);

        findViewById(R.id.login).setOnClickListener(this);
        findViewById(R.id.register).setOnClickListener(this);
    }

    public static String token;
    boolean isEmail(EditText text) {
        CharSequence email = text.getText().toString();
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    void checkDataEntered() {
        if (isEmpty(usernameEditText)) {
            Toast t = Toast.makeText(this, "You must enter username to login!", Toast.LENGTH_SHORT);
            t.show();
        }

        if (isEmpty(passwordEditText)) {
            passwordEditText.setError("password is required!");
        }
    }
    private void userLogin(View v) {
        String username = usernameEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();

        Call<User> call = RetrofitClient
                .getInstance().getUserClient().userLogin(new Login(username, password));
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                checkDataEntered();
                if (response.isSuccessful()) {
                    //Toast.makeText(LoginActivity.this, response.body().getAuthToken(), Toast.LENGTH_SHORT).show();
                    System.out.println(response.body());

                    token = response.body().getAuthToken();
                    usernameHeader = username;
                    emailHeader = response.body().getEmail();
                    imageHeader = response.body().getPhotoURL();
                    user = response.body();
                    id = response.body().getId();
                    //System.out.println(token);
                    homeActivityPage(v);
                } else {
                    Toast.makeText(LoginActivity.this, "Login incorrect", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void registrationActivityPage(View view) {
        startActivity(new Intent(this, RegistrationActivity.class));
    }

    public void homeActivityPage(View view) {

        Intent MainIntent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(MainIntent);
    }

}

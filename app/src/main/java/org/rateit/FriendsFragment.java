package org.rateit;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rateit.R;

import org.rateit.Models.FriendRequest;
import org.rateit.Models.User;
import org.rateit.Models.UserProposition;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;

public class FriendsFragment extends Fragment{
    private RecyclerView recyclerViewOne;
    private RecyclerView recyclerViewTwo;
    private RecyclerView recyclerViewThree;
    private List<FriendRequest> friendRequests;
    private List<UserProposition> userPropositions;
    private List<User> users;
    private FriendRequestAdapter friendRequestAdapter;
    private FriendPropositionsAdapter friendPropositionsAdapter;
    private MyFriendAdapter myFriendAdapter;
    private TextView noFriends;
    private TextView noPropostions;
    private TextView noRequests;
    private TextView myFriends;
    private TextView myRequests;
    private TextView myPropositions;
    private UserSearchingAdapter userSearchingAdapter;
    private RecyclerView recyclerViewTwoForSearch;
    int spanCount = 3; // 3 columns
    int spacing = 50; // 50px
    boolean includeEdge = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.multiple_relative_view, container, false);
        noFriends = view.findViewById(R.id.empty_view_three);
        noPropostions = view.findViewById(R.id.empty_view_two);
        noRequests = view.findViewById(R.id.empty_view_one);
        myFriends = view.findViewById(R.id.textViewFriends);
        myPropositions = view.findViewById(R.id.textViewPropositions);
        myRequests = view.findViewById(R.id.textViewRequests);
        recyclerViewOne = view.findViewById(R.id.recyclerOne);
        recyclerViewThree = view.findViewById(R.id.recyclerThree);
        recyclerViewTwoForSearch = view.findViewById(R.id.recyclerViewForSearch);
        recyclerViewTwoForSearch.setLayoutManager(new GridLayoutManager(container.getContext(), spanCount, RecyclerView.VERTICAL, false));
        recyclerViewTwoForSearch.setNestedScrollingEnabled(false);
        recyclerViewTwoForSearch.setHasFixedSize(true);
        LinearLayoutManager layoutManagerOne = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewOne.setLayoutManager(layoutManagerOne);
        recyclerViewOne.setHasFixedSize(true);
        recyclerViewTwo = view.findViewById(R.id.recyclerTwo);
        LinearLayoutManager layoutManagerTwo = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewTwo.setLayoutManager(layoutManagerTwo);
        recyclerViewTwo.setHasFixedSize(true);
        //LinearLayoutManager layoutManagerThree = new LinearLayoutManager(getActivity(), GridLayoutManager., false);
        recyclerViewThree.setLayoutManager(new GridLayoutManager(container.getContext(), spanCount, RecyclerView.VERTICAL, false));
        recyclerViewThree.setHasFixedSize(true);

        recyclerViewOne.setNestedScrollingEnabled(false);
        recyclerViewTwo.setNestedScrollingEnabled(false);
        recyclerViewThree.setNestedScrollingEnabled(true);


        searchUser("");
        friendsRequestResponse();
        friendsProposalResponse();
        myFriendsResponse();


        return view;
    }

    public void friendsRequestResponse() {
        Call<List<FriendRequest>> call = RetrofitClient
                .getInstance().getUserClient().getMyRequests(token);
        call.enqueue(new Callback<List<FriendRequest>>() {
            @Override
            public void onResponse(Call<List<FriendRequest>> call, Response<List<FriendRequest>> response) {
                recyclerViewTwoForSearch.setVisibility(View.GONE);
                if (!response.isSuccessful()) {
//                    textViewResult.setText("Code: " + response.code());
                    System.out.println("Code: " + response.code());
                    return;
                } else {
                    if (friendRequests == null) {
                        friendRequests = new ArrayList<FriendRequest>();
                        friendRequests = response.body();
                    } else {
                        friendRequests = response.body();
                    }
                    if (!friendRequests.isEmpty()) {
                        noRequests.setVisibility(View.GONE);
                        recyclerViewOne.setVisibility(View.VISIBLE);
                        friendRequestAdapter = new FriendRequestAdapter(friendRequests, getActivity());
                        recyclerViewOne.setAdapter(friendRequestAdapter);
                        friendRequestAdapter.notifyDataSetChanged();
                    } else {
                        recyclerViewOne.setVisibility(View.GONE);
                        noRequests.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<FriendRequest>> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
                System.out.println("NIE poszlo");
                System.out.println(t.getMessage());
            }
        });


    }

    public void friendsProposalResponse() {
        Call<List<UserProposition>> call = RetrofitClient
                .getInstance().getUserClient().getUserPropositions(token);
        call.enqueue(new Callback<List<UserProposition>>() {
            @Override
            public void onResponse(Call<List<UserProposition>> call, Response<List<UserProposition>> response) {
                recyclerViewTwoForSearch.setVisibility(View.GONE);
                if (!response.isSuccessful()) {
//                    textViewResult.setText("Code: " + response.code());
                    System.out.println("Code: " + response.code());
                    return;
                } else {
                    if (userPropositions == null) {
                        userPropositions = new ArrayList<UserProposition>();
                        userPropositions = response.body();
                    } else {
                        userPropositions = response.body();
                    }
                    System.out.println(userPropositions.toString());
                    if (!userPropositions.isEmpty()) {
                        noPropostions.setVisibility(View.GONE);
                        recyclerViewTwo.setVisibility(View.VISIBLE);
                        friendPropositionsAdapter = new FriendPropositionsAdapter(userPropositions, getActivity());
                        recyclerViewTwo.setAdapter(friendPropositionsAdapter);
                        friendPropositionsAdapter.notifyDataSetChanged();
                    } else {
                        recyclerViewTwo.setVisibility(View.GONE);
                        noPropostions.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<UserProposition>> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
                System.out.println("NIE poszlo");
                System.out.println(t.getMessage());
            }
        });
    }

    public void myFriendsResponse() {
        Call<List<User>> call = RetrofitClient
                .getInstance().getUserClient().getMyFriends(token);
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (!response.isSuccessful()) {
//                    textViewResult.setText("Code: " + response.code());
                    System.out.println("Code: " + response.code());
                    return;
                } else {
                    if (users == null) {
                        users = new ArrayList<User>();
                        users = response.body();
                    } else {
                        users = response.body();
                    }
                    if (!users.isEmpty()) {
                        noFriends.setVisibility(View.GONE);
                        recyclerViewThree.setVisibility(View.VISIBLE);
                        myFriendAdapter = new MyFriendAdapter(users, getActivity());
                        recyclerViewThree.setAdapter(myFriendAdapter);
                        myFriendAdapter.notifyDataSetChanged();
                    } else {
                        recyclerViewThree.setVisibility(View.GONE);
                        noFriends.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
                System.out.println("NIE poszlo");
                System.out.println(t.getMessage());
            }
        });
    }

    public void searchUser(String name) {
        if (name.equals("")) {
            myFriends.setVisibility(View.VISIBLE);
            myPropositions.setVisibility(View.VISIBLE);
            myRequests.setVisibility(View.VISIBLE);
            friendsRequestResponse();
            friendsProposalResponse();
            myFriendsResponse();
        } else {

            Call<List<User>> call = RetrofitClient
                    .getInstance().getUserClient().searchUserByNick(token, name);

            call.enqueue(new Callback<List<User>>() {
                @Override
                public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                    recyclerViewTwoForSearch.setVisibility(View.GONE);
                    if (!response.isSuccessful()) {
//                    textViewResult.setText("Code: " + response.code());
                        System.out.println("Code: " + response.code());
                        return;
                    } else {
                        UserSearchingAdapter userSearchingAdapter = new UserSearchingAdapter();
                        if (response.body() == null) {
                            return;
                        }
                        /*if(userSearchingAdapter == null) {
                            users = response.body();
                            userSearchingAdapter = new UserSearchingAdapter(users, getActivity());
                        }*/
                        else {
                            myFriends.setVisibility(View.GONE);
                            myPropositions.setVisibility(View.GONE);
                            myRequests.setVisibility(View.GONE);
                            noFriends.setVisibility(View.GONE);
                            noPropostions.setVisibility(View.GONE);
                            noRequests.setVisibility(View.GONE);
                            recyclerViewTwo.setVisibility(View.GONE);
                            recyclerViewThree.setVisibility(View.GONE);
                            recyclerViewOne.setVisibility(View.GONE);
                            recyclerViewTwoForSearch.setVisibility(View.VISIBLE);
                            List<User> users = response.body();
                            userSearchingAdapter = new UserSearchingAdapter(users, getActivity());
                            recyclerViewTwoForSearch.setAdapter(userSearchingAdapter);
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<User>> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
                    System.out.println("NIE poszlo");
                    System.out.println(t.getMessage());
                }
            });
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();

        inflater.inflate(R.menu.menu_item, menu);
        MenuItem item = menu.findItem(R.id.search);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItem.SHOW_AS_ACTION_IF_ROOM);

        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                searchUser(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // Here is where we are going to implement the filter logic
                searchUser(newText);
                return true;
            }

        });
    }

}

package org.rateit;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rateit.R;
import com.squareup.picasso.Picasso;

import org.rateit.Models.FriendRequest;
import org.rateit.Models.User;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;

public class FriendRequestAdapter extends RecyclerView.Adapter<FriendRequestAdapter.MyViewHolder> {
    private Context context;
    private List<FriendRequest> list;

    public FriendRequestAdapter() {
    }

    public FriendRequestAdapter(List<FriendRequest> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_recieve_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //holder.button.setId(position);
        Picasso.get().load(list.get(position).getRequester().getPhotoURL()).into(holder.proposalFriendAvatar);
        holder.receiverUsername.setText(list.get(position).getRequester().getUsername());
        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                long requesterId = list.get(position).getRequester().getId();
                System.out.println(requesterId);
                Call<ResponseBody> call = RetrofitClient
                        .getInstance()
                        .getUserClient()
                        .replyToRequest(token, requesterId, "ACCEPTED");
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        String s = null;
                        try {
                            if (!response.isSuccessful()) {
                                s = response.errorBody().string();
                            }
                            else {
                                list.remove(position);
                                FriendRequestAdapter.this.notifyDataSetChanged();
                                Toast.makeText(context, "Request accepted", Toast.LENGTH_SHORT).show();
                                System.out.println(response.code());
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        System.out.println(":(");
                    }
                });
            }
        });
        holder.decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                long requesterId = list.get(position).getRequester().getId();
                System.out.println(requesterId);
                Call<ResponseBody> call = RetrofitClient
                        .getInstance()
                        .getUserClient()
                        .replyToRequest(token, requesterId, "DENIED");
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        String s = null;
                        try {
                            if (!response.isSuccessful()) {
                                s = response.errorBody().string();
                            }
                            else {
                                list.remove(position);
                                FriendRequestAdapter.this.notifyDataSetChanged();
                                Toast.makeText(context, "Request declined", Toast.LENGTH_SHORT).show();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        System.out.println(":(");
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }



    public static class MyViewHolder extends RecyclerView.ViewHolder {

        Button accept, decline;
        TextView receiverUsername;
        ImageView proposalFriendAvatar;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            accept = itemView.findViewById(R.id.accept_friend);
            decline = itemView.findViewById(R.id.decline_friend);
            receiverUsername = itemView.findViewById(R.id.receiver_nick);
            proposalFriendAvatar = itemView.findViewById(R.id.proposal_friend_avatar);

        }
    }

    public void friendsDetailsPage(User user) {
        Intent intent = new Intent(context, FriendsDetailsActivity.class);
        intent.putExtra("USER", user);
        context.startActivity(intent);
    }
}

package org.rateit;


import org.rateit.Models.CD;
import org.rateit.Models.CDAllResponse;
import org.rateit.Models.CDResponse;
import org.rateit.Models.ChangeEmail;
import org.rateit.Models.ChangePassword;
import org.rateit.Models.Chat;
import org.rateit.Models.Comments;
import org.rateit.Models.FriendRequest;
import org.rateit.Models.Login;
import org.rateit.Models.NewMessage;
import org.rateit.Models.OwnedAlbumMap;
import org.rateit.Models.Rate;
import org.rateit.Models.RatedCD;
import org.rateit.Models.Registration;
import org.rateit.Models.User;
import org.rateit.Models.UserProposition;
import org.rateit.Models.UserWishlist;
import org.rateit.Models.Wishlist;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UserClient {
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("api/signup")
    Call<User> createUser(
            @Body Registration user
    );

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("login")
    Call<User> userLogin(
            @Body Login login
    );

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("/api/cds/{id}/add-comment")
    Call<Comments> addComment(
            @Header("Authorization") String authToken,
            @Body Comments comments,
            @Path("id") long id);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("/api/cds/{id}/rate")
    Call<Rate> addRate(
            @Header("Authorization") String authToken,
            @Path("id") long id,
            @Query("note") float note,
            @Query("userId") Long userId);

    @Headers({"Content-Type: application/json"})
    @GET("/api/{object}/search")
    Call<CDAllResponse> searchCDByName(
            @Header("Authorization") String authToken,
            @Path("object") String object,
            @Query("q") String q
    );

    @Headers({"Content-Type: application/json"})
    @GET("/api/{object}")
    Call<CDAllResponse> searchCDByArtist(
            @Header("Authorization") String authToken,
            @Path("object") String object,
            @Query("artist") String artist
    );

    @Headers({"Content-Type: application/json"})
    @GET("/api/{object}")
    Call<CDAllResponse> searchCDByDate(
            @Header("Authorization") String authToken,
            @Path("object") String object,
            @Query("released") String released
    );

    @Headers({"Content-Type: application/json"})
    @GET("/api/user-by-nick")
    Call<List<User>> searchUserByNick(
            @Header("Authorization") String authToken,
            @Query("nick") String nick
    );

    @Headers({"Content-Type: application/json"})
    @GET("/api/{object}/{id}")
    Call<CDResponse> getDataCDById(
            @Header("Authorization") String authToken,
            @Path("object") String object,
            @Path("id") long id);

    @Headers({"Content-Type: application/json"})
    @GET("api/{object}")
    Call<CDAllResponse> getDataCD(
            @Header("Authorization") String authToken,
            @Path("object") String object);

    @Headers({"Content-Type: application/json"})
    @GET("api/{object}?size=50&")
    Call<CDAllResponse> getDataCDWithPagination(
            @Header("Authorization") String authToken,
            @Path("object") String object,
            @Query("page") int page);

    @Headers({"Content-Type: application/json"})
    @GET("api/{object}/ranking")
    Call<List<CD>> getRankingCD(
            @Header("Authorization") String authToken,
            @Path("object") String object);

    @Headers({"Content-Type: application/json"})
    @GET("api/{object}/{id}/rated")
    Call<List<RatedCD>> getRatedCD(
            @Header("Authorization") String authToken,
            @Path("object") String object,
            @Path("id") Long id);

    @Headers({"Content-Type: application/json"})
    @GET("api/{object}/{id}/cds")
    Call<List<OwnedAlbumMap>> getOwnedCD(
            @Header("Authorization") String authToken,
            @Path("object") String object,
            @Path("id") Long id);

    @Headers({"Content-Type: application/json"})
    @POST("api/{object}/{id}/add-cd/{cd-id}")
    Call<User> postOwnedCD(
            @Header("Authorization") String authToken,
            @Path("object") String object,
            @Path("id") Long id,
            @Path("cd-id") long cd_id,
            @Body User user,
            @Query("type") String type);

    @Headers({"Content-Type: application/json"})
    @DELETE("api/{object}/owned-cds/{id}")
    Call<ResponseBody> deleteCDFromOwned(
            @Header("Authorization") String authToken,
            @Path("object") String object,
            @Path("id") long id);

    @Headers({"Content-Type: application/json"})
    @POST("/api/request/{requesterId}/seen")
    Call<User> checkAsSeen(
            @Header("Authorization") String authToken,
            @Path("requesterId") long requesterId);

    @Headers({"Content-Type: application/json"})
    @POST("/api/requests/{requesterId}/reply")
    Call<ResponseBody> replyToRequest(
            @Header("Authorization") String authToken,
            @Path("requesterId") long requesterId,
            @Query("status") String status);

    @Headers({"Content-Type: application/json"})
    @POST("/api/users/{receiverId}/send-request")
    Call<FriendRequest> sendRequest(
            @Header("Authorization") String authToken,
            @Path("receiverId") long receiverId,
            @Body FriendRequest friendRequest);

    @Headers({"Content-Type: application/json"})
    @GET("/api/users/{id}/friends")
    Call<List<User>> getUserFriends(
            @Header("Authorization") String authToken,
            @Path("id") long id);

    @Headers({"Content-Type: application/json"})
    @GET("/api/users/my-friends")
    Call<List<User>> getMyFriends(
            @Header("Authorization") String authToken);

    @Headers({"Content-Type: application/json"})
    @GET("/api/users/friendsPropositions")
    Call<List<UserProposition>> getUserPropositions(
            @Header("Authorization") String authToken);

    @Headers({"Content-Type: application/json"})
    @GET("/api/users/my-requests")
    Call<List<FriendRequest>> getMyRequests(
            @Header("Authorization") String authToken);

    @Headers({"Content-Type: application/json"})
    @DELETE("api/friends/{friendId}")
    Call<ResponseBody> deleteFriend(
            @Header("Authorization") String authToken,
            @Path("friendId") long friendId);

    @Headers({"Content-Type: application/json"})
    @GET("api/chats")
    Call<List<Chat>> getMyChats(
            @Header("Authorization") String authToken);

    @Headers({"Content-Type: application/json"})
    @POST("api/chats")
    Call<Chat> newChat(
            @Header("Authorization") String authToken,
            @Query("personId") long personId,
            @Body Chat chat);

    @Headers({"Content-Type: application/json"})
    @GET("api/chats/{personId}")
    Call<Chat> getOneChat(
            @Header("Authorization") String authToken,
            @Path("personId") long personId);

    @Headers({"Content-Type: application/json"})
    @POST("api/chats/check-as-seen")
    Call<Chat> chceckMessageAsSeen(
            @Header("Authorization") String authToken,
            @Body Chat chat);

    @Headers({"Content-Type: application/json"})
    @POST("api/chats/newMessage")
    Call<Chat> writeNewMessage(
            @Header("Authorization") String authToken,
            @Body NewMessage newMessage);

    @Headers({"Content-Type: application/json"})
    @POST("/api/logout")
    Call<ResponseBody> logout(
            @Header("Authorization") String authToken);
    @Headers({"Content-Type: application/json"})
    @POST("/wish-list")
    Call<Wishlist> addToWishlist(
            @Header("Authorization") String authToken,
            @Query("cdId") long cdId,
            @Query("type") String type);
    @Headers({"Content-Type: application/json"})
    @GET("/users/{id}/wish-list")
    Call<UserWishlist> getUserWishlist(
            @Header("Authorization") String authToken,
            @Path("id") long id);
    @Headers({"Content-Type: application/json"})
    @DELETE("/wish-list/{wish-id}")
    Call<ResponseBody> deleteFromWishlist(
            @Header("Authorization") String authToken,
            @Path("wish-id") long wishId);
    @Headers({"Content-Type: application/json"})
    @POST("/api/users/{user-id}/give/{owned-cd-id}")
    Call<OwnedAlbumMap> giveCD(
            @Header("Authorization") String authToken,
            @Path("user-id") long userId,
            @Path("owned-cd-id") long ownedCdId);
    @Headers({"Content-Type: application/json"})
    @POST("/api/users/{id}/change-email")
    Call<ChangeEmail> editEmail(
            @Header("Authorization") String authToken,
            @Path("id") long id,
            @Body ChangeEmail changeEmail);
    @Headers({"Content-Type: application/json"})
    @POST("/api/users/{id}/change-password")
    Call<ChangePassword> editPassword(
            @Header("Authorization") String authToken,
            @Path("id") long id,
            @Body ChangePassword changePassword);
    @Headers({"Content-Type: application/json"})
    @GET("/api/users/{id}")
    Call<User> getUsersById(
            @Header("Authorization") String authToken,
            @Path("id") long id);
    @Headers({"Content-Type: application/json"})
    @GET("/api/users/{id}/cds")
    Call<List<OwnedAlbumMap>> getUserCd(
            @Header("Authorization") String authToken,
            @Path("id") long id);

}

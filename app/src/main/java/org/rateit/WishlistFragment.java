package org.rateit;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.rateit.R;
import com.squareup.picasso.Picasso;

import org.rateit.Models.CDResponse;
import org.rateit.Models.UserWishlist;
import org.rateit.Models.Wishlist;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;

public class WishlistFragment extends Fragment {
    public RatingBar ratingBar;
    private List<Wishlist> wishlists;
    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_view, container, false);
        //addListenerOnRatingBar(view);
        listView = (ListView) view.findViewById(R.id.listView);
        responseWishlist();
        return view;
    }

    public void responseWishlist() {
        Call<UserWishlist> call = RetrofitClient
                .getInstance().getUserClient().getUserWishlist(token, LoginActivity.id);

        call.enqueue(new Callback<UserWishlist>() {
            @Override
            public void onResponse(Call<UserWishlist> call, Response<UserWishlist> response) {
                if (!response.isSuccessful()) {
//                    textViewResult.setText("Code: " + response.code());
                    System.out.println("Code: " + response.code());
                    return;
                } else {
                    UserWishlist userWishlist = response.body();
                    if (userWishlist != null) {
                        wishlists = new ArrayList<>();
                        wishlists = userWishlist.getWishlists();
                        CustomAdapter customAdapter = new CustomAdapter(wishlists);
                        listView.setAdapter(customAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<UserWishlist> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
                System.out.println("NIE poszlo");
                System.out.println(t.getMessage());
            }
        });
    }

    public class CustomAdapter extends BaseAdapter {
        private List<Wishlist> list;

        public CustomAdapter(List<Wishlist> list) {
            this.list = list;
        }

        @Override
        public int getCount() {
            if (list != null)
                return list.size();
            else
                return 0;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            ViewHolder holder;
                convertView = getLayoutInflater().inflate(R.layout.fragment_wishlist, null);
                holder = new ViewHolder();


                holder.artistTextView = (TextView) convertView.findViewById(R.id.artistName);
                holder.cdNameTextView = (TextView) convertView.findViewById(R.id.cdName);
                holder.imageView = (ImageView) convertView.findViewById(R.id.CDImage);
                holder.ratingBarView = (RatingBar) convertView.findViewById(R.id.ratingBar);
                holder.deleteButton = (Button) convertView.findViewById(R.id.delete_button);
                holder.detailsButton = (Button) convertView.findViewById(R.id.detailsButton);
                holder.cdTypeTextView = (TextView) convertView.findViewById(R.id.cd_type);

                holder.cdTypeTextView.setText(wishlists.get(i).getCdType().getType());
            holder.cdNameTextView.setText(wishlists.get(i).getCd().getName());
            Picasso.get().load(wishlists.get(i).getCd().getPhotoURL()).into(holder.imageView);
            holder.ratingBarView.setRating(wishlists.get(i).getCd().getRating());
            holder.artistTextView.setText(wishlists.get(i).getCd().getArtist());

                holder.detailsButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        long cdId = list.get(i).getCd().getId();
                        Call<CDResponse> call = RetrofitClient
                                .getInstance()
                                .getUserClient()
                                .getDataCDById(token, "cds", cdId);
                        call.enqueue(new Callback<CDResponse>() {
                            @Override
                            public void onResponse(Call<CDResponse> call, Response<CDResponse> response) {
                                String s = null;
                                try {
                                    if (!response.isSuccessful()) {
                                        s = response.errorBody().string();
                                    }
                                    CDResponse cd = response.body();
                                    detailsActivityPage(cd);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Call<CDResponse> call, Throwable t) {
                                System.out.println(":(");
                            }
                        });
                    }
                });


                holder.deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        long cdId = wishlists.get(i).getId();
                        Call<ResponseBody> call = RetrofitClient
                                .getInstance()
                                .getUserClient()
                                .deleteFromWishlist(token, cdId);
                        call.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                System.out.println(response.code());
                                if(response.isSuccessful()) {
                                    responseWishlist();
                                    Toast.makeText(getActivity(), "Delete from wishlist succesfully", Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
                                System.out.println("NIE poszlo");
                                System.out.println(t.getMessage());
                            }
                        });
                    }
                });

                convertView.setTag(holder);
            return convertView;
        }
    }

    static class ViewHolder {
        Button deleteButton, detailsButton;
        TextView artistTextView, cdNameTextView, cdTypeTextView;
        ImageView imageView;
        RatingBar ratingBarView;
    }

    public void detailsActivityPage(CDResponse cd) {
        Intent intent = new Intent(getActivity(), CDDetailsActivity.class);
        intent.putExtra("CD", cd);
        startActivity(intent);
    }
}

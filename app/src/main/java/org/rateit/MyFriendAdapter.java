package org.rateit;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rateit.R;
import com.squareup.picasso.Picasso;

import org.rateit.Models.Chat;
import org.rateit.Models.User;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.rateit.LoginActivity.token;

public class MyFriendAdapter extends RecyclerView.Adapter<MyFriendAdapter.MyViewHolder> {
    private Context context;
    private List<User> list;


    public MyFriendAdapter() {
    }

    public MyFriendAdapter(List<User> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public void newChat(User participant, int position) {
        Call<Chat> call = RetrofitClient
                .getInstance()
                .getUserClient()
                .newChat(token, participant.getId(), new Chat(LoginActivity.user, null, participant));
        call.enqueue(new Callback<Chat>() {
            @Override
            public void onResponse(Call<Chat> call, Response<Chat> response) {
                String s = null;
                try {
                    if (!response.isSuccessful()) {
                        s = response.errorBody().string();
                    } else {
                        getOneChat(position);
                    }
                    System.out.println(response.code());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Chat> call, Throwable t) {
                System.out.println(":(");
            }
        });
    }

    public void getOneChat(int position) {
        long userId = list.get(position).getId();
        User participant = list.get(position);
        Call<Chat> call = RetrofitClient
                .getInstance()
                .getUserClient()
                .getOneChat(token, userId);
        call.enqueue(new Callback<Chat>() {
            @Override
            public void onResponse(Call<Chat> call, Response<Chat> response) {
                String s = null;
                try {
                    if (!response.isSuccessful()) {
                        s = response.errorBody().string();
                    } else {
                        messagesActivityPage(response.body());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Chat> call, Throwable t) {
                System.out.println(":(");
            }
        });
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_friend_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //holder.button.setId(position);
        Picasso.get().load(list.get(position).getPhotoURL()).into(holder.myFriendAvatar);
        holder.myFriendUsername.setText(list.get(position).getUsername());
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                long userId = list.get(position).getId();
                Call<ResponseBody> call = RetrofitClient
                        .getInstance()
                        .getUserClient()
                        .deleteFriend(token, userId);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        String s = null;
                        try {
                            if (!response.isSuccessful()) {
                                s = response.errorBody().string();
                            }else {
                                list.remove(position);
                                MyFriendAdapter.this.notifyDataSetChanged();
                                System.out.println(response.code());
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        System.out.println(":(");
                    }
                });
            }
        });
        holder.message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                long userId = list.get(position).getId();
                User participant = list.get(position);
                Call<Chat> call = RetrofitClient
                        .getInstance()
                        .getUserClient()
                        .getOneChat(token, userId);
                call.enqueue(new Callback<Chat>() {
                    @Override
                    public void onResponse(Call<Chat> call, Response<Chat> response) {
                        String s = null;
                        try {
                            if (!response.isSuccessful()) {
                                s = response.errorBody().string();
                            }
                            if (response.body() != null) {
                                messagesActivityPage(response.body());
                            } else {
                                newChat(participant, position);
                            }
                            System.out.println(response.code());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<Chat> call, Throwable t) {
                        System.out.println(":(");
                    }
                });
            }
        });
        holder.details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<User> call = RetrofitClient
                        .getInstance()
                        .getUserClient()
                        .getUsersById(token, list.get(position).getId());
                call.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        String s = null;
                        try {
                            if (!response.isSuccessful()) {
                                s = response.errorBody().string();
                            }
                            if (response.body() != null) {
                                friendsDetailsPage(response.body());
                            }
                            System.out.println(response.code());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        System.out.println(":(");
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        Button delete, message, details;
        TextView myFriendUsername;
        ImageView myFriendAvatar;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            delete = itemView.findViewById(R.id.delete_friend_button);
            message = itemView.findViewById(R.id.write_friend_message);
            myFriendUsername = itemView.findViewById(R.id.my_friend_nick);
            myFriendAvatar = itemView.findViewById(R.id.my_friend_avatar);
            details = itemView.findViewById(R.id.user_details);
        }
    }

    public void friendsDetailsPage(User user) {
        Intent intent = new Intent(context, FriendsDetailsActivity.class);
        intent.putExtra("USER", user);
        context.startActivity(intent);
    }

    public void messagesActivityPage(Chat chat) {
        Intent intent = new Intent(context, MessagingActivity.class);
        intent.putExtra("CHAT", chat);
        context.startActivity(intent);
    }
}
